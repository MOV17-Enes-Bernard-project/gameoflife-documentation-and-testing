# Documentation and Testing of Game Of Life #
## What is this repository for? ##

1. Documentation of the program
[`Game of Life`](https://bitbucket.org/Erik_Strom/gameoflife) by [EricStröm](https://bitbucket.org/Erik_Strom)
2. Writing unit  tests for the said program

## How do I get set up? ##
#### Summary of set up ####
- All header files are found in the `include` and `terminal` folders and corresponding source files in `src` folder.
- Documentation files are found in `docs` folder.
- Test files are found in the `test` folder.

## Configuration ##
Setup should be like this
```
 * .gitignore
 * .gitmodules
 * CMakeLists.txt
 * Population_Seed.txt
 * README.md (this file)
 * docs/
     * images    (All images for the project inside here)
     * Doxyfile  (The Doxygen setup file)
 * test/
     * test-fileXXX.cpp 
     * test-fileXXX.cpp (where test-fileXXX represent a test file for a particular class.)
     * test_seed.txt (a file containing some test seeds)
 * include
 * src    
 * terminal
```
 

## How to run tests ##
* Use separate test file per tested class
* Use BDD style when writing tests as it is easier to read i.e. specify ```SCENARIO, GIVEN, WHEN``` and ```THEN```

## Code review guidelines ##
* Verify if all the tests done are documented
* Verify that all the documented tests are implemented

## Other guidelines ##
* In the Trello board follow the manifest for marking the tasks that are test related
* Include the 'Test Checklist' for each test task

## Who do I talk to? ##
Bernard Longho <lobe1602@student.miun.se>

Enes Bajramovic <enba1200@student.miun.se>
