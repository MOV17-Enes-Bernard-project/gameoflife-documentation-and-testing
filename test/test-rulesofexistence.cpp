/**
 * @file test-rulesofexistence.cpp
 * @ingroup Test_Files
 * @brief Unit tests for the public functions of the RuleOfExistence implementation classes
 * @date 28-Oct-17
 * @author Enes Bajramovic
 */

#include "catch.hpp"
#include <map>
#include <Cell_Culture/Cell.h>
#include <Support/SupportStructures.h>
#include <GoL_Rules/RuleOfExistence_Erik.h>
#include <GoL_Rules/RuleOfExistence_Conway.h>
#include <GoL_Rules/RuleOfExistence_VonNeumann.h>

/**
 * @brief Make some test cells
 * @param cells Container with cells and their positions
 */
void populateCells(std::map<Point, Cell> &cells){
    cells[Point{0,0}] = Cell(true);
    cells[Point{1,0}] = Cell(true);
    cells[Point{2,0}] = Cell(true);
    cells[Point{3,0}] = Cell(true);
    cells[Point{4,0}] = Cell(true);

    cells[Point{0,1}] = Cell(true);
    cells[Point{1,1}] = Cell(false, IGNORE_CELL);
    Point checkPoint1 = Point{2,1};
    cells[checkPoint1] = Cell(false, IGNORE_CELL);
    cells[Point{3,1}] = Cell(false, IGNORE_CELL);
    cells[Point{4,1}] = Cell(true);

    cells[Point{0,2}] = Cell(true);
    cells[Point{1,2}] = Cell(false, GIVE_CELL_LIFE);
    Point checkPoint2 = Point{2,2};
    cells[checkPoint2] = Cell(false, GIVE_CELL_LIFE);
    cells[Point{3,2}] = Cell(false, GIVE_CELL_LIFE);
    cells[Point{4,2}] = Cell(true);

    cells[Point{0,3}] = Cell(true);
    cells[Point{1,3}] = Cell(false, IGNORE_CELL);
    Point checkPoint3 = Point{2,3};
    cells[checkPoint3] = Cell(false, IGNORE_CELL);
    cells[Point{3,3}] = Cell(false, IGNORE_CELL);
    cells[Point{4,3}] = Cell(true);

    cells[Point{0,4}] = Cell(true);
    cells[Point{1,4}] = Cell(true);
    cells[Point{2,4}] = Cell(true);
    cells[Point{3,4}] = Cell(true);
    cells[Point{4,4}] = Cell(true);
}
/**
 * @brief Unit All the RuleOfExistence
 */
SCENARIO("Testing the RuleOf Existence implementations"){
    GIVEN("the population of the cells") {
        Point checkPoint1 = Point{2, 1};
        Point checkPoint2 = Point{2, 2};
        Point checkPoint3 = Point{2, 3};

        WHEN("having a new instance of a conway rule") {
            std::map<Point, Cell> cells;
            populateCells(cells);
            RuleOfExistence *rule = new RuleOfExistence_Conway(cells);
            THEN("the correct name should be returned") {
                REQUIRE(rule->getRuleName() == "conway");
            }
            AND_WHEN("the rule is executed") {
                rule->executeRule();
                THEN("the rule should be correctly applied") {
                    for (auto it = cells.begin(); it != cells.end(); it++) {
                        if (!(it->first < checkPoint1) && !(checkPoint1 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() ==
                                    GIVE_CELL_LIFE);// previously was IGNORE_CELL
                        if (!(it->first < checkPoint2) && !(checkPoint2 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() ==
                                    IGNORE_CELL); // previously was GIVE_CELL_LIFE
                        if (!(it->first < checkPoint3) && !(checkPoint3 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() ==
                                    GIVE_CELL_LIFE); // previously was IGNORE_CELL
                    }
                }
            }
            delete rule;
        }


        WHEN("having a new instance of VonNeumann rule") {
            std::map<Point, Cell> cells;
            populateCells(cells);
            RuleOfExistence *rule = new RuleOfExistence_VonNeumann(cells);
            THEN("the correct name should be returned") {
                REQUIRE(rule->getRuleName() == "von_neumann");
            }
            AND_WHEN("the rule is executed") {
                rule->executeRule();
                THEN("the rule should be correctly applied") {
                    for (auto it = cells.begin(); it != cells.end(); it++) {
                        if (!(it->first < checkPoint1) && !(checkPoint1 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() == DO_NOTHING);// previously was IGNORE_CELL
                        if (!(it->first < checkPoint2) && !(checkPoint2 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() ==
                                    IGNORE_CELL); // previously was GIVE_CELL_LIFE
                        if (!(it->first < checkPoint3) && !(checkPoint3 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() == DO_NOTHING); // previously was IGNORE_CELL
                    }
                }
            }
            delete rule;
        }

        WHEN("having a new instance of Erik rule") {
            std::map<Point, Cell> cells;
            populateCells(cells);
            RuleOfExistence *rule = new RuleOfExistence_Erik(cells);

            //hack the cells to activate 'erikfying'
            for (int i = 0; i < 5; i++) {
                for (auto it = cells.begin(); it != cells.end(); it++) {
                    if (it->second.isAlive()) {
                        it->second.setNextGenerationAction(IGNORE_CELL);
                        it->second.updateState();
                    }
                }
            }
            THEN("the correct name should be returned") {
                REQUIRE(rule->getRuleName() == "erik");
            }
            AND_WHEN("the rule is executed") {
                rule->executeRule();
                THEN("the rule should be correctly applied") {
                    for (auto it = cells.begin(); it != cells.end(); it++) {
                        if (!(it->first < checkPoint1) && !(checkPoint1 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() ==
                                    GIVE_CELL_LIFE);// previously was IGNORE_CELL
                        if (!(it->first < checkPoint2) && !(checkPoint2 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() ==
                                    IGNORE_CELL); // previously was GIVE_CELL_LIFE
                        if (!(it->first < checkPoint3) && !(checkPoint3 < it->first))
                            REQUIRE(it->second.getNextGenerationAction() ==
                                    GIVE_CELL_LIFE); // previously was IGNORE_CELL
                    }
                }
            }
            AND_WHEN("the rule is executed after five generations") {
                rule->executeRule(); // sets the next generation color to OLD for cells older than 5 y
                THEN("cells older than 5 years may be erikfied -> color turned to old") {
                    for (auto it = cells.begin(); it != cells.end(); it++) {
                        if (it->second.getAge() > 5 && it->second.getNextGenerationAction() != KILL_CELL) {
                            it->second.updateState(); //needed to apply the next color to the old cell
                            REQUIRE(it->second.getColor() == STATE_COLORS.OLD);
                        }
                    }
                }
            }
            AND_WHEN("the rule is executed after 10 generations ") {
                //hack the cells to activate 'erikfying'
                for (int i = 0; i < 5; i++) {
                    for (auto it = cells.begin(); it != cells.end(); it++) {
                        if (it->second.isAlive()) {
                            it->second.setNextGenerationAction(IGNORE_CELL);
                            it->second.updateState();
                        }
                    }
                }
                rule->executeRule();
                int elderCounter = 0;
                THEN("cells older than 10 years may be erikfied -> cell value turned to 'E' and only one set to prime elder") {
                    for (auto it = cells.begin(); it != cells.end(); it++) {
                        if (it->second.getAge() > 10 && it->second.getNextGenerationAction() != KILL_CELL) {
                            it->second.updateState(); //needed to apply the 'E' value to the old cell
                            REQUIRE(it->second.getCellValue() == 'E');
                            if (it->second.getColor() == STATE_COLORS.ELDER)
                                elderCounter++;
                        }
                    }
                    REQUIRE(elderCounter == 1);
                }
            }
            delete rule;
        }
    }
}