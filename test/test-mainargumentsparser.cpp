/**
 * @file test-mainargumentsparser.cpp
 * @ingroup Test_Files
 * @brief Unit tests for the public functions of the MainArgumentsParser class
 * @date 28-Oct-17
 * @author Enes Bajramovic
 */
#include "catch.hpp"
#include "Support/MainArgumentsParser.h"
#include "Support/FileLoader.h"

/**
 * @brief Testing the MainArgumentsParser
 */
SCENARIO("MainArgumentsParser parsing arguments") {
    GIVEN("the application arguments"){
        ApplicationValues appValues;
        MainArgumentsParser parser;

        WHEN("not provided with arguments"){
            char  arg0[] = "GameOfLife";
            char* argv[] = { &arg0[0], NULL };
            int   argc   = (int)(sizeof(argv) / sizeof(argv[0])) - 1;
            THEN("even and odd rules should equal 'conway' and generations should be 100, run simulation should be active"){
                appValues = parser.runParser(&argv[0], argc);
                REQUIRE(appValues.maxGenerations == 100);
                REQUIRE(appValues.evenRuleName == "conway");
                REQUIRE(appValues.oddRuleName == "conway" );
                REQUIRE(appValues.runSimulation);
            }
        }

        WHEN("provided with -h argument"){
            char  arg0[] = "GameOfLife";
            char  arg1[] = "-h";
            char* argv[] = { &arg0[0], &arg1[0], NULL };
            int   argc   = (int)(sizeof(argv) / sizeof(argv[0])) - 1;
            appValues = parser.runParser(&argv[0], argc);
            THEN("simulation should not be run"){
                REQUIRE_FALSE(appValues.runSimulation);
            }
        }


        WHEN("provided with dimension and generations arguments"){

            char  arg0[] = "GameOfLife";
            char  arg1[] = "-s";
            char  arg2[] = "80x24";
            char  arg3[] = "-g";
            char  arg4[] = "200";
            char* argv[] = { &arg0[0], &arg1[0], &arg2[0], &arg3[0], &arg4[0], NULL };
            int   argc   = (int)(sizeof(argv) / sizeof(argv[0])) - 1;
            appValues = parser.runParser(&argv[0], argc);
            THEN("even and odd rules should equal 'conway', and generations and world dimensions should match"){
                REQUIRE(appValues.maxGenerations == 200);
                REQUIRE(appValues.evenRuleName == "conway");
                REQUIRE(appValues.oddRuleName == "conway" );
                REQUIRE(appValues.runSimulation);
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 80);
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
            }
        }
        WHEN("provided with dimension and even rule arguments = 'erik'"){
            char  arg0[] = "GameOfLife";
            char  arg1[] = "-s";
            char  arg2[] = "160x24";
            char  arg3[] = "-er";
            char  arg4[] = "erik";
            char* argv[] = { &arg0[0], &arg1[0], &arg2[0], &arg3[0], &arg4[0], NULL };
            int   argc   = (int)(sizeof(argv) / sizeof(argv[0])) - 1;
            appValues = parser.runParser(&argv[0], argc);
            THEN("even and odd rules should equal 'erik', and world dimensions should match"){
                REQUIRE(appValues.maxGenerations == 100);
                REQUIRE(appValues.evenRuleName == "erik");
                REQUIRE(appValues.oddRuleName == "erik" );
                REQUIRE(appValues.runSimulation);
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 160);
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 24);
            }
        }

        WHEN("provided with dimension and odd rule argument = 'conway' even rule arguments = 'erik' and filename"){
            char  arg0[] = "GameOfLife";
            char  arg1[] = "-s";
            char  arg2[] = "160x24";
            char  arg3[] = "-er";
            char  arg4[] = "conway";
            char  arg5[] = "-or";
            char  arg6[] = "erik";
            char  arg7[] = "-f";
            char  arg8[] = "test_seed.txt";
            char  arg9[] = "-g";
            char  arg10[] = "2000";
            char* argv[] = { &arg0[0], &arg1[0], &arg2[0], &arg3[0], &arg4[0], &arg5[0],
                             &arg6[0], &arg7[0], &arg8[0], &arg9[0], &arg10[0], NULL };
            int   argc   = (int)(sizeof(argv) / sizeof(argv[0])) - 1;
            appValues = parser.runParser(&argv[0], argc);
            FileLoader loader;
            map<Point, Cell> cellsLoadedFromFile;
            loader.loadPopulationFromFile(cellsLoadedFromFile);
            THEN("even rule should match 'conway' and odd rule should equal 'erik', "
                         "world dimensions should be loaded from the mock file"){
                REQUIRE(appValues.maxGenerations == 2000);
                REQUIRE(appValues.evenRuleName == "conway");
                REQUIRE(appValues.oddRuleName == "erik" );
                REQUIRE(appValues.runSimulation);
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 4);
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 3);
            }
            fileName.clear();
        }
    }
}