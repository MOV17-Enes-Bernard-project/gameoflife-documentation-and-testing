/**
 * @file test-main.cpp
 * @ingroup Test_Files
 * @brief Unit test main class
 * @date 25-Oct-17
 * @author Enes Bajramovic / Bernard Che Longho
 */
#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#ifdef DEBUG
#include <memstat.hpp>
#endif

#include <memory>
