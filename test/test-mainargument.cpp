/**
 * @file test-mainargument.cpp
 * @ingroup Test_Files
 * @brief Unit tests for the public functions of the BaseArgument implementation classes
 * @date 28-Oct-17
 * @author Enes Bajramovic
 */
#include "catch.hpp"
#include "Support/MainArguments.h"

/**
 * @brief Testing that BaseARgument correctly assigns a value to ApplicationValue
 */
SCENARIO("BaseArgument implementations correctly assign value to ApplicationValue") {
    ApplicationValues appValues;
    GIVEN("the help argument instance") {
        unique_ptr<BaseArgument> argument(new HelpArgument());

        WHEN("instance compared to expected ApplicationValue parameters") {
            char *value;
            argument->execute(appValues, value);
            THEN("the argument literal should match the provided one and run simulation parameter should be off") {
                REQUIRE(argument->getValue() == "-h");
                REQUIRE(!appValues.runSimulation);
            }
        }
    }
    GIVEN("the generations argument instance") {
	    unique_ptr<BaseArgument> argument(new GenerationsArgument());

        WHEN("instance provided with the value for the generations argument") {
            char value[] = "400";
            appValues.maxGenerations = 100;
            argument->execute(appValues, value);
            THEN("generations should match the provided literal and run "
		                 "simulation should be on") {
                REQUIRE(argument->getValue() == "-g");
                REQUIRE(appValues.maxGenerations == 400);
                REQUIRE(appValues.runSimulation);
            }
        }
        WHEN("instance not provided with the value for the argument") {
            char* value = nullptr;
            appValues.runSimulation = true;
            argument->execute(appValues, value);
            THEN("run simulation should be off") {
                REQUIRE(!appValues.runSimulation);
            }
        }
    }
    GIVEN("the world-size argument instance") {
	    unique_ptr<BaseArgument> argument(new WorldsizeArgument());

        WHEN("instance provided with the value for the generations argument") {
            char value[] = "40x20";
            appValues.maxGenerations = 100;
            argument->execute(appValues, value);
            THEN("dimensions should match the provided literal and run simulation should be on") {
                REQUIRE(argument->getValue() == "-s");
                REQUIRE(WORLD_DIMENSIONS.WIDTH == 40);
                REQUIRE(WORLD_DIMENSIONS.HEIGHT == 20);
                REQUIRE(appValues.runSimulation);
            }
        }
        WHEN("instance not provided with the value for the argument") {
            char* value = nullptr;
            appValues.runSimulation = true;
            argument->execute(appValues, value);
            THEN("run simulation should be off") {
                REQUIRE(!appValues.runSimulation);
            }
        }
    }

    GIVEN("the file argument instance") {
	    unique_ptr<BaseArgument> argument(new FileArgument());

        WHEN("instance not provided with the value for the argument") {
            char* value = nullptr;
            appValues.runSimulation = true;
            argument->execute(appValues, value);

            THEN("run simulation should be off") {
                REQUIRE(!appValues.runSimulation);
            }
            AND_THEN("file name should not be set"){
                REQUIRE(fileName.empty());
            }
        }

        WHEN("instance provided with the value for the file name argument") {
            char value[] = "name.txt";
            appValues.runSimulation = true;
            argument->execute(appValues, value);
            THEN("file name should match the provided literal and"
		                 " run simulation should be on") {
                REQUIRE(argument->getValue() == "-f");
                REQUIRE(fileName == "name.txt");
                REQUIRE(appValues.runSimulation);
            }
        }
    }

    GIVEN("the even rule argument instance") {
	    unique_ptr<BaseArgument> argument(new EvenRuleArgument());

        WHEN("instance provided with the value for the generations argument") {
            char value[] = "bobolibo";
            argument->execute(appValues, value);
            THEN("even rule name should match the provided literal and run "
		                 "simulation should be on") {
                REQUIRE(argument->getValue() == "-er");
                REQUIRE(appValues.evenRuleName == "bobolibo");
                REQUIRE(appValues.runSimulation);
            }
        }
        WHEN("instance not provided with the value for the argument") {
            char* value = nullptr;
            appValues.runSimulation = true;
            argument->execute(appValues, value);
            THEN("run simulation should be off and evenRuleName should be "
		                 "empty") {
                REQUIRE(!appValues.runSimulation);
	            REQUIRE(appValues.evenRuleName == "");
            }
        }
    }

    GIVEN("the even rule argument instance") {
	    unique_ptr<BaseArgument> argument(new OddRuleArgument());

	    WHEN("instance provided with the value for the generations argument") {
            char value[] = "bobolibo";
            argument->execute(appValues, value);
            THEN("dimensions should match the provided literal and run simulation should be on") {
                REQUIRE(argument->getValue() == "-or");
                REQUIRE(appValues.oddRuleName == "bobolibo");
                REQUIRE(appValues.runSimulation);
            }
        }
        WHEN("instance not provided with the value for the argument") {
            char* value = nullptr;
            appValues.runSimulation = true;
            argument->execute(appValues, value);
            THEN("run simulation should be off and the value oddRuleName "
		                 "should be empty") {
                REQUIRE(!appValues.runSimulation);
	            REQUIRE(appValues.oddRuleName == "");
            }
        }
    }
}
