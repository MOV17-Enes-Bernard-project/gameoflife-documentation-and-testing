/**
 * @file test-cell.cpp
 * @ingroup Test_Files
 * @author Bernard Longho
 * @date 30-Oct-17
 * @brief Testing public member functions of the Cell class
 */
#include "catch.hpp"
#include "Cell_Culture/Cell.h"

/**
 * @brief Testing the different cell properties
 */
SCENARIO("Trying to alter the states of a cell") {
    GIVEN("a cell with no action taken") {
        WHEN("it is rim") {
            Cell cell(true);

            THEN("isRimCell should be true and its original age is zero") {
                REQUIRE(cell.isRimCell());
                REQUIRE(cell.getAge() == 0);

                AND_THEN("the cell is death and has the dead color") {
                    REQUIRE_FALSE(cell.isAlive());
                    REQUIRE(cell.getColor() == STATE_COLORS.DEAD);
                }
            }

            WHEN("attempting to give the cell life") {
                ACTION preAction = cell.getNextGenerationAction();
                cell.setNextGenerationAction(ACTION::GIVE_CELL_LIFE);

                THEN("the cell's current state will not change") {
                    ACTION postAction = cell.getNextGenerationAction();

                    REQUIRE_FALSE(cell.isAlive());
                    REQUIRE(cell.getAge() == 0);
                    REQUIRE(cell.getColor() == STATE_COLORS.DEAD);
                    REQUIRE(preAction == postAction);
                }
                AND_WHEN("attempting to change the cell color") {
                    cell.setNextColor(STATE_COLORS.LIVING);

                    THEN("the color will remain the same") {
                        REQUIRE(cell.getColor() == STATE_COLORS.DEAD);
                    }
                    AND_WHEN("trying to set a value for the cell") {

                        char previousValue = cell.getCellValue();
                        cell.setNextCellValue('*');

                        char currentValue = cell.getCellValue();

                        THEN("the original value of the cell will not change") {
                            REQUIRE(previousValue == currentValue);
                        }
                    }
                }
            }
        }
    }
    GIVEN("a cell with no action taken") {
        WHEN("it is a non-rim cell") {
            Cell nonRim(false);

            THEN("the program should report true that it is not rim") {
                REQUIRE_FALSE(nonRim.isRimCell());
            }

            AND_THEN("action should be DO_NOTHING") {
                REQUIRE(nonRim.getNextGenerationAction() == ACTION::DO_NOTHING);
            }
            AND_THEN("the cell's age should be zero") {
                REQUIRE(nonRim.getAge() == 0);
            }
            AND_THEN("the cell is dead since age is zero") {
                REQUIRE_FALSE(nonRim.isAlive());
                REQUIRE(nonRim.getColor() == STATE_COLORS.DEAD);
            }
            AND_WHEN("setting the cell's next generation to give life") {
                nonRim.setNextGenerationAction(ACTION::GIVE_CELL_LIFE);

                THEN("action should now be GIVE_CELL_LIFE") {
                    REQUIRE(nonRim.getNextGenerationAction() == ACTION::GIVE_CELL_LIFE);
                }
                AND_WHEN("cell is given a LIVING color") {
                    nonRim.setNextColor(STATE_COLORS.LIVING);
                    nonRim.updateState();

                    THEN("the cell's state is now alive") {
                        REQUIRE(nonRim.isAlive());
                    }
                    AND_THEN("the cell now has a living color") {
                        REQUIRE(nonRim.getColor() == STATE_COLORS.LIVING);
                    }
                    AND_THEN("the cell's age is 1") {
                        REQUIRE(nonRim.getAge() == 1);
                    }
                    AND_WHEN("the cell is updated with a new value") {
                        char preValue = nonRim.getCellValue();

                        nonRim.setNextCellValue('E');
                        nonRim.updateState();
                        char postValue = nonRim.getCellValue();

                        THEN("the cell's has the newly set value") {
                            REQUIRE_FALSE(preValue == postValue);
                            REQUIRE(postValue == 'E');
                        }
                        AND_WHEN("the cell's action is set to kill") {
                            nonRim.setNextGenerationAction(ACTION::KILL_CELL);

                            THEN("the cell's action is now kill_cell") {
                                REQUIRE(nonRim.getNextGenerationAction() == ACTION::KILL_CELL);
                            }
                            AND_THEN("the cell's age is returned to 0 after updating") {
                                nonRim.updateState();
                                REQUIRE_FALSE(nonRim.isAlive());
                                REQUIRE(nonRim.getAge() == 0);
                            }
                            AND_THEN("the cell is not alive after the previous update") {
                                REQUIRE_FALSE(nonRim.isAliveNext());
                            }
                            AND_WHEN("the cell is forced into being alive") {
                                nonRim.setIsAliveNext(true);

                                THEN("then the cell will be alive") {
                                    REQUIRE(nonRim.isAliveNext());
                                }
                                AND_THEN("the cell's age is gracefully added") {
                                    REQUIRE(nonRim.getAge() == 1);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
















