/**
 * @file helper.h
 * @ingroup Test_Files
 * @brief File with some helper functions for testing
 * @author Bernard Che Longho
 * @date 02-Nov-17
 */
#ifndef GAMEOFLIFE_HELPER_H
#define GAMEOFLIFE_HELPER_H
#include "../include/Cell_Culture/Cell.h"

/**
 * @brief Compare two cells and check if they are equal in all respects
 * @param c1 Cell1
 * @param c2 Cell2
 * @return True if both Cell1 and Cell2 have the same characteristics
 */
bool isSameCell(Cell &c1, Cell &c2){
    return
            c1.getAge() == c2.getAge() &&
            c1.isRimCell() == c2.isRimCell() &&
            c1.isAlive() == c2.isAlive() &&
            c1.getColor() == c2.getColor() &&
            c1.getCellValue() == c2.getCellValue() &&
            c1.getNextGenerationAction() == c2.getNextGenerationAction() &&
            c1.isAliveNext() == c2.isAliveNext();

}
#endif //GAMEOFLIFE_HELPER_H
