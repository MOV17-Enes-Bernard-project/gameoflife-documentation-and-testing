/**
 * @file test-fileloader.cpp
 * @ingroup Test_Files
 * @brief Unit tests for the public functions of the FileLoader class
 * @date 25-Oct-17
 * @author Enes Bajramovic
 */

#include "catch.hpp"
#include "Support/FileLoader.h"
#include "Support/MainArguments.h"
#include <Cell_Culture/Population.h>
#include <algorithm>
#include <map>

/**
 * @brief Testing that the fileloader correctly reads cells from file
 */
SCENARIO("FileLoader loading seeds and creating cells and dimensions") {
    GIVEN("the hardcoded population seed") {
        map<Point, Cell> testCells;
        testCells[Point{0, 0}] = Cell(true);
        testCells[Point{1, 0}] = Cell(true);
        testCells[Point{2, 0}] = Cell(true);
        testCells[Point{3, 0}] = Cell(true);
        testCells[Point{4, 0}] = Cell(true);
        testCells[Point{5, 0}] = Cell(true);

        testCells[Point{0, 1}] = Cell(true);
        testCells[Point{1, 1}] = Cell(false, IGNORE_CELL);
        testCells[Point{2, 1}] = Cell(false, IGNORE_CELL);
        testCells[Point{3, 1}] = Cell(false, IGNORE_CELL);
        testCells[Point{4, 1}] = Cell(false, IGNORE_CELL);
        testCells[Point{5, 1}] = Cell(true);

        testCells[Point{0, 2}] = Cell(true);
        testCells[Point{1, 2}] = Cell(false, IGNORE_CELL);
        testCells[Point{2, 2}] = Cell(false, GIVE_CELL_LIFE);
        testCells[Point{3, 2}] = Cell(false, GIVE_CELL_LIFE);
        testCells[Point{4, 2}] = Cell(false, IGNORE_CELL);
        testCells[Point{5, 2}] = Cell(true);

        testCells[Point{0, 3}] = Cell(true);
        testCells[Point{1, 3}] = Cell(false, IGNORE_CELL);
        testCells[Point{2, 3}] = Cell(false, IGNORE_CELL);
        testCells[Point{3, 3}] = Cell(false, GIVE_CELL_LIFE);
        testCells[Point{4, 3}] = Cell(false, IGNORE_CELL);
        testCells[Point{5, 3}] = Cell(true);

        testCells[Point{0, 4}] = Cell(true);
        testCells[Point{1, 4}] = Cell(true);
        testCells[Point{2, 4}] = Cell(true);
        testCells[Point{3, 4}] = Cell(true);
        testCells[Point{4, 4}] = Cell(true);
        testCells[Point{5, 4}] = Cell(true);

        WHEN("seeds are loaded from the file") {
            AND_WHEN("a correct file name is given") {
                char fileName[] = "test_seed.txt";
                ApplicationValues appValues;
                BaseArgument *arg = new FileArgument();
                arg->execute(appValues, &fileName[0]);
                delete arg;

                FileLoader loader;
                map<Point, Cell> cellsLoadedFromFile;

                THEN("there should not be a throw") {
                    REQUIRE_NOTHROW(loader.loadPopulationFromFile(cellsLoadedFromFile));
                }

                AND_THEN("cells should be loaded.") {
                    loader.loadPopulationFromFile(cellsLoadedFromFile);

                    REQUIRE(cellsLoadedFromFile.size() == testCells.size());

                    int width = 4;
                    int height = 3;
                    AND_THEN("dimensions should be equal") {
                        REQUIRE(WORLD_DIMENSIONS.WIDTH == width);
                        REQUIRE(WORLD_DIMENSIONS.HEIGHT == height);
                    }

                    AND_THEN("loaded cells should be equal to test cells") {
                        map<Point, Cell>::iterator testCellIt;
                        map<Point, Cell>::iterator loadedCellIt;
                        for (loadedCellIt = cellsLoadedFromFile.begin(), testCellIt = testCells.begin();
                             loadedCellIt != cellsLoadedFromFile.end();
                             ++loadedCellIt, ++testCellIt) {

                            REQUIRE(loadedCellIt->second.getAge() == testCellIt->second.getAge());
                            REQUIRE(loadedCellIt->second.getColor() == testCellIt->second.getColor());
                            REQUIRE(loadedCellIt->second.isRimCell() == testCellIt->second.isRimCell());
                        }
                    }
                }
            }
            AND_WHEN("a non-existent filename is given") {
                char file[] = "testing_seed.txt";
                ApplicationValues appValues;
                BaseArgument *arg = new FileArgument();
                arg->execute(appValues, &file[0]);
                delete arg;

                FileLoader loader;
                map<Point, Cell> cellsLoadedFromFile;
                THEN("an error is thrown") {
                    REQUIRE_THROWS(loader.loadPopulationFromFile(cellsLoadedFromFile));

                    fileName.clear();
                }
            }
        }
    }
}
