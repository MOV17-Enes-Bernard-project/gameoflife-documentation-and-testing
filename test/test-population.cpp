/**
 * @file test-population.cpp
 * @ingroup Test_Files
 * @brief Unit tests for the public functions of the Population class
 * @date 02-Nov-17
 * @author Bernard Che Longho
 */
#include "catch.hpp"
#include "Cell_Culture/Population.h"
#include "helper.h"
#include <iostream>

/**
 * @brief Testing that Population correctly populates the cell culture
 */
SCENARIO("Population correctly gets creates a cell culture, calculates number of cells and gets generation count") {
    GIVEN("an empty cell population") {
        Population emptyPop;

        WHEN("the population has not been initiated") {
            THEN("the cell population is zero") {
                REQUIRE(emptyPop.getTotalCellPopulation() == 0);
            }
            AND_WHEN("attempting to get a cell in the population") {
                THEN("an exception should be thrown") {
                    REQUIRE_THROWS(emptyPop.getCellAtPosition({0, 0}));
                }
            }
        }
    }
    GIVEN("a cell population to be initiated") {
        Population pop;

        WHEN("no rule is applied and fileName is empty") {
            fileName = "";
            pop.initiatePopulation("");

            THEN("a random generation will be created with the default dimensions") {
                REQUIRE(pop.getTotalCellPopulation() ==
                        (WORLD_DIMENSIONS.HEIGHT + 2) * (WORLD_DIMENSIONS.WIDTH + 2));
            }
            AND_WHEN("attempting to get a cell in the culture") {
                THEN("an error should not be thrown") {
                    REQUIRE_NOTHROW(pop.getCellAtPosition({0, 0}));
                }
                AND_THEN("the cells' generation should increase") {
                    int gen{0};
                    REQUIRE(pop.calculateNewGeneration() == ++gen);
                    REQUIRE(pop.calculateNewGeneration() == ++gen);
                    REQUIRE(pop.calculateNewGeneration() == ++gen);

                }
            }
        }
        WHEN("the file name is specified but no rule") {
            fileName = "test_seed.txt";
            /*
                4x3
                0000
                0110
                0010
             */
            pop.initiatePopulation("");

            THEN("total population should be (4+2) * (3+2)[30]") {
                REQUIRE(pop.getTotalCellPopulation() == 30);
            }
            AND_THEN("one should get a correct cell in the world") {
                REQUIRE_NOTHROW(pop.getCellAtPosition({3, 3}));

                Cell cell = pop.getCellAtPosition({3, 3}); // gets 1
                Cell rimCell = pop.getCellAtPosition({3, 4}); // gets 0 at the edge
                Cell cell2 = pop.getCellAtPosition({1, 3}); // gets 0 which is not at edge and not rim

                REQUIRE_FALSE(cell.isRimCell());
                REQUIRE(cell.isAlive());

                REQUIRE(rimCell.isRimCell());
                REQUIRE_FALSE(rimCell.isAlive());

                REQUIRE_FALSE(cell2.isRimCell());
                REQUIRE_FALSE(cell2.isAlive());
            }
            AND_THEN("the cell generation should increase") {
                int generation{0};
                REQUIRE(pop.calculateNewGeneration() == ++generation);
                REQUIRE(pop.calculateNewGeneration() == ++generation);
                REQUIRE(pop.calculateNewGeneration() == ++generation);
                REQUIRE(pop.calculateNewGeneration() == ++generation);
            }
            AND_WHEN("attempting to get a non-existent cell") {
                THEN("an error should be thrown") {
                    REQUIRE_THROWS(pop.getCellAtPosition({1, 5})); // no cell exists
                }
            }
            fileName.clear();
        }
    }
    GIVEN("two cell populations initiated with different rules") {
        Population erikPop, vonPop;
        WHEN("the new generations for each are generated") {
            erikPop.initiatePopulation("erik");
            erikPop.calculateNewGeneration(); 

            vonPop.initiatePopulation("von_neumann");
            vonPop.calculateNewGeneration();
            THEN("the cells should be different since they have different rules") {
                bool notSameCell = false;
                for (int row = 0; row < WORLD_DIMENSIONS.HEIGHT + 1; row++) {
                    for (int col = 0; col < WORLD_DIMENSIONS.WIDTH + 1; col++) {

                        auto erik = erikPop.getCellAtPosition({row, col});
                        auto con = vonPop.getCellAtPosition({row, col});
                        if (!isSameCell(erik, con)) {
                            notSameCell = true;
                        }
                    }
                }
                REQUIRE(notSameCell);
            }
        }
    }
}