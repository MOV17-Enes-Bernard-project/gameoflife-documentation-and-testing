/**
 * @file test-RuleFactory.cpp
 * @author Bernard Che Longho
 * @ingroup Test_Files
 * @brief Testing the RuleFactory class
 * @date 2-Nov-2017
 */
#include "catch.hpp"
#include "GoL_Rules/RuleFactory.h"

/**
 * @brief Unit testing the public functions of RuleFactory
 */
SCENARIO("Instantiating a rule of existence") {
    GIVEN("that no rule is specified yet") {
        WHEN("trying to get an instance of a rule") {
            RuleFactory &firstFactory = RuleFactory::getInstance();
            RuleFactory &secondFactory = RuleFactory::getInstance();

            THEN("a reference to the instance of the same kind of rule is returned") {
                REQUIRE(&firstFactory == &secondFactory);
            }
        }
    }
    GIVEN("that one has the intention to create a rule") {

        vector<string> rules{"erik", "von_neumann", "conway"};
        map<Point, Cell> cells;

        WHEN("no rule name is specified") {
            RuleFactory &factory = RuleFactory::getInstance();

            THEN("conway rule is applied by default") {
                RuleOfExistence *defaultRule = factory.createAndReturnRule(cells, rules[2]);

                REQUIRE(defaultRule->getRuleName() == "conway");

                delete defaultRule;
            }
        }
        AND_WHEN("rule names are specified") {
            RuleFactory &factory_1 = RuleFactory::getInstance();
            RuleFactory &factory_2 = RuleFactory::getInstance();
            RuleFactory &factory_3 = RuleFactory::getInstance();

            RuleOfExistence *rule_1 = factory_1.createAndReturnRule(cells, rules[0]);
            RuleOfExistence *rule_2 = factory_2.createAndReturnRule(cells, rules[1]);
            RuleOfExistence *rule_3 = factory_3.createAndReturnRule(cells, rules[2]);

            THEN("the correct rule should be created") {
                REQUIRE(rule_1->getRuleName() == "erik");
                REQUIRE(rule_2->getRuleName() == "von_neumann");
                REQUIRE(rule_3->getRuleName() == "conway");
                delete rule_1;
                delete rule_2;
                delete rule_3;
            }

        }
    }
}