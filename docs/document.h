/**
 * @file document.h
 * @brief File with al the documentation headers
 * @author Bernard Che Longho
 * @date 1 Nov 2017
 */
#ifndef GAMEOFLIFE_DOCUMENT_H
#define GAMEOFLIFE_DOCUMENT_H

// WHAT TO DISPLAY AS index.html OF THE PROGRAM DOCUMENTATION
/**
 * @mainpage The Game of Life
 * @brief This file covers the documentation of Game of Life <BR>
 * @details The Game of life is implemented to mimmick the cellular
 * automation<BR> by [John H. Conwaw](https://en.wikipedia
 * .org/wiki/John_Horton_Conway) who established an example of cellular <BR>
 * automation. The classic rules for the program is that
 * - Each cell is in two possible states
 * 	- alive (populated)
 * 	- death (unpopulated)
 * - Every cell interacts with its 8 neighbours
 * - At anytime,
 * 	<UL>
 * 		<LI> A live cell with fewer than 2 neighbours dies (underpopulation)
 * 		</LI>
 * 		<LI> Any live cell with 2 or 3 neighbours lives on </LI>
 * 		<LI> Any live cell with more than three live neigbours dies </LI>
 * 		<LI>Any dead cell with exactly 2 live neighbours becomes alive </LI>
 * 	</UL>
 * 	<BR>Implementation of this documentation was done by @author Erik Ström
 * 	<BR> Documentation and test writing was done by @ref authorsPage
 * 	@see
 * 	Read more about
 * 	[the game of life.](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
 * */

// PROGRAM MODULES/GROUP DEFINITIONS
/**
 * @defgroup Cell_Culture Classes and functions for the Cell and population
 * This groups all the modules related to the cell culture
 * @}
 */

/**
 * @defgroup Rule_of_Existence Classes and functions governing rule of existence
 * This group defines all the modules related to the rules that govern the
 * state of each cell in the simulation program
 * @}
 */

/**
 * @defgroup Support Support files
 * This group defines all the modules related to the rules that govern the
 * state of each cell in the simulation program
 * @}
 */

/**
 * @defgroup Simulation Core simulation classes and functions
 * This group contains the main implementation of the game of life - the
 * simulation program itself
 * @}
 */

/**
 * @defgroup Test_Files All files that have the testing codes
 * This group contains all the tests that have been made in this project
 * @}
 */


// Author page
/**
 * @page authorsPage The authors
 * This work is brought to you by two awesome guys
 * @section authorsSection Authors
 * @subsection bernardNameSubSection Bernard Che Longho
 * @author Bernard Che Longho <lobe1602@student.miun.se>
 * @image html Bernard.png
 * Bernard is a 32 year old married guy with two kids born in Jan 2015 and October 2017. <BR>
 * He lives in Umeå and is studying Software Engineering from Distance.
 *
 * @subsection enesNameSubSection Enes Bajramovic
 * @image html enes.jpg
 * Enes is a 42 years old living in Huddinge with wife and two kids aged 12 and 9.
 * Working full time and studying the last year of Computer Engineering on free time.
 * @author Enes
 */
#endif //GAMEOFLIFE_DOCUMENT_H
