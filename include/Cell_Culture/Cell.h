/**
 * @file    Cell.h
 * @ingroup Cell_Culture
 * @author  Erik Ström
 * @date    October 2017
 * @version ver. 0.1
 * @brief About cell state and properties during the whole simulation process
*/

#ifndef cellH
#define cellH

#include "../../terminal/terminal.h"

/**
 * @struct StateColors
 * @ingroup Cell_Culture
 * @brief Different states of the a cell in Cell_Culture
 * @details Different states that identify each cell at any time in the program
 * */
struct StateColors {
    COLOR LIVING, /**< Representing living */
            DEAD, /**< Representing dead cell */
            OLD,  /**< Representing old cell */
            ELDER; /**< Representing very old cell */
}

/**
 * @brief Define default colors for the different cell states
 * @details COLOR is an enum class from Terminal.h. It defines the color printed
 * in the terminal.
 *
 */
const STATE_COLORS = {COLOR::WHITE, COLOR::BLACK, COLOR::CYAN, COLOR::MAGENTA};

/**
 * @enum ACTION
 * @brief What happens to the cells as defined by the rules
 * @details Cells can either be killed, ignored, given life or let to be at
 * its current state. All these are determined by the rules of existence
 * @ingroup Cell_Culture
 */
enum ACTION { KILL_CELL, IGNORE_CELL, GIVE_CELL_LIFE, DO_NOTHING };

/**
 * @class Cell
 * @ingroup Cell_Culture
 * @brief Cell characteristics
 * @details Cells represents a certain combination of row and column of the
 * simulated world. <BR>
 * Cells may be of two types; rim cells, those representing the outer limits of the world,
or non-rim cells. <BR> The first cell type are immutable, exempt from the game's rules, and
thus their values may not be changed. <BR>The latter type, however, may be
 changed and edited in ways specified by the rules.
 */
class Cell {

private:
    /**
     * @struct CellDetails
     * @brief Cell detailed characteristics
     * @ingroup Cell_Culture
     */
    struct CellDetails {
        int age; /**< The age of the cell */
        COLOR color; /**< Cell color as defined in STATE_COLORS */
        bool rimCell; /**< true if cell is rim cell */
        char value; /**< Character to represent cell on the screen */
    } details; /**< An object of CellDetails */

    /**
     * @struct NextUpdate
     * @brief Cell states
     * @details contains some more detail information tracking the cells as the
     * move from one state to another.
     * @ingroup Cell_Culture
     */
    struct NextUpdate {
        ACTION nextGenerationAction; /**< What happens to a cell  */
        COLOR nextColor; /**< Next state of the cell */
        char nextValue; /**< Based on cell's state, it gets a different value*/
        bool willBeAlive; /**< Cell can either be death or alive */
    } nextUpdate; /**< An object of NextUpdate */

    /**
     * @brief Add the age of the cell
     */
    void incrementAge() { details.age++; }

    /**
     * @brief reset cell's age to zero
     */
    void killCell() { details.age = 0; }

    /**
     * @brief Character to represent cell on the screen
     * @param value
     */
    void setCellValue(char value) { details.value = value; }

    /**
     * @brief Set the color of the cell based on its state
     * @param color
     */
    void setColor(COLOR color) { this->details.color = color; }

public:

    /**
     * @brief Default constructor of a cell
     * @param isRimCell
     * @param action
     * @details The program begins with every cell in its original state and
     * until something happens, nothing is done to this state.
     */
    Cell(bool isRimCell = false, ACTION action = DO_NOTHING);

    /**
     * @brief Assertain the state of the cell
     * @return The state of the cell (death or alive)
     * @test If the cell is alive, this should report true
     * @test A rim cell will always report false irrespective of action taken
     */
    bool isAlive();

    /**
     * @brief sets the cell's next action to take in its coming update
     * @param action Action to be done on the next generation
     * @test When an action is set for a rim cell and the cell state updated, the cell should have the new action
     * @test Rim cell's action is unaltered after update
     */
    void setNextGenerationAction(ACTION action);

    /**
     * @brief Update the cell to its new state based on store values i.e
     * either kill, or increment cell's age
     * @test When a cell undergoes a change, updateState() should update the state of the cell
     */
    void updateState();

    /**
     * @brief Get the age of a cell
     * @return A cell's age
     * @test The cell's age should be gotten after an action is done on the cell
     * @test If rim cell, the age will always report to zero
     */
    int getAge() { return details.age; }

    /**
     * @brief  Get the color of a cell
     * @return The cell's color
     * @test getColor() should correctly report the color of the cell when set and updated
     */
    COLOR getColor() { return details.color; }

    /**
     * @brief Determines whether the cell is a rim cell, and thus should be immutable
     * @return True if cell is rim cell. Otherwise false.
     * @test If cell is rim, this should report to true otherwise false
     */
    bool isRimCell() { return details.rimCell; }

    /**
     * @brief Sets the color the cell will have after its next update.
     * @param nextColor The color destined for the cell after the next upate
     * @test If cell is non-rim, then cell should get the color specified.
     * Otherwise, the color does not change
     */
    void setNextColor(COLOR nextColor) { this->nextUpdate.nextColor = nextColor; }

    /**
     * @brief get the cell value
     * @return The character value to represent the cell in the terminal
     * @test A rim cell's value never changes
     * @test Non-rim cell's value should change after set and updated
     */
    char getCellValue() { return details.value; }

    /**
     * @brief Sets the next character value of the cell, which will be printed to screen.
     * @param value The value the cell will assume after next update
     * @test That a non-rim cell should correctly get a new value after update
     * @test That a rim cell's value never changes after set and updated
     */
    void setNextCellValue(char value) { nextUpdate.nextValue = value; }

    /**
     * @brief Set that the cell should be alive after next update
     * @param isAliveNext whether or not cell will be alive after next updated
     * @test Give live to a dead cell
     * @test if cell is rim, nothing happens. Otherwise cell becomes alive
     */
    void setIsAliveNext(bool isAliveNext) { nextUpdate.willBeAlive = isAliveNext; }

    /**
     * @brief Check if the cell will be alive after the next action?
     * @return future state of the cell
     * @details Assertain whether or not the cell will be alive based on the
     * game rules (checking the surrounding neighbours state
     * @test That a cell's status is correctly reported as being alive after update
     */
    bool isAliveNext() { return nextUpdate.willBeAlive; }

    /**
     * @brief Gets the cells next action.
     * @return one of the values of ACTION
     * @test Correctly get the action set for a cell in the next generation
     * @test A non-rim cell's action never changes
     */
    ACTION& getNextGenerationAction() { return nextUpdate.nextGenerationAction; }

};

#endif