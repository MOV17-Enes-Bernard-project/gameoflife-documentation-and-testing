/**
 * @file Population.h
 * @ingroup Cell_Culture
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
*/
#ifndef POPULATION_H
#define POPULATION_H

#include<map>
#include<string>
#include "Cell.h"
#include "Support/Globals.h"
#include "GoL_Rules/RuleOfExistence.h"
#include "GoL_Rules/RuleFactory.h"

using namespace std;

/**
 * @class Population
 * @ingroup Cell_Culture
 * @brief Determine which rule to apply for each new generation
 * @details Representation of the complete society of cell existence and
 * interaction.<br>The Population constitutes of all current, previous and
 * future generations of cells, both living and dead as well as those not yet
 * born.<br> By mapping all cells to their respective positions in the
 * simulation world, Population has the complete knowledge of each cell's
 * whereabouts. <br>Furthermore, the class is responsible for
 *  determining which rules should be required from the RuleFactory, and
 *  store the pointer to these as members.<br>
 *  Population's main responsibility during execution is determining which rule
 *  to apply for each new generation and updating the cells to their new states.
*/
class Population
{
private:
    int generation; /**< Number of cells present in the culture */
    map<Point, Cell> cells; /**< Cells and their coordinates */
    RuleOfExistence* evenRuleOfExistence; /**< An instance of evenRuleOfExistence */
    RuleOfExistence* oddRuleOfExistence; /**< An instance of oddRuleOfExistence */

	/**
	 * @brief Build the cell culture with randomized starting values
	 */
    void randomizeCellCulture();

	/**
	 * @brief Send cells map to FileLoader, which will populate its culture based on file values.
	 * @test Given a file with values, build a culture @result number of
	 * cells should match that which we have from file
	 */
    void buildCellCultureFromFile();

public:
	/**
	 * @brief At the beginning of the program, the cell population is at zero.
	 * @test Test to confirm that when program starts, cell population is
	 * zero before initiation.
	 * @test after cells are populated, their number should be different from 0.
	 */
    Population() : generation(0), evenRuleOfExistence(nullptr), oddRuleOfExistence(nullptr) {}

	/**
	 * @brief Free allocated memories during population initiation
	 */
    ~Population();

	/**
	 * @brief Initialize cell culture
	 * @details Based on the rules (from the RuleFactory), cells are
	 * generated and the culture is populated
	 * @param evenRuleName Apply even rule while generating the cell culture
	 * @param oddRuleName Apply odd rule while generating the cell culture
	 * @test Initiate population with no rule and no file name and check that it worked
	 * @test Initiate population with fileName and check that it worked
	 * @test Initiate population with different rules and check that the cells are different
	 * after a new generation is calculated
	 */
    void initiatePopulation(string evenRuleName, string oddRuleName = "");

	/**
	 * @brief Update the cell population and determine next generational
	 * changes based on RuleOfExistence
	 * @return updated cell count in the culture
	 * @test an initiated population's generation should increase
	 */
    int calculateNewGeneration();

	/**
	 * @brief Returns cell by specified key value.
	 * @param position (Point)
	 * @return cell att position
	 * @test Check a position in loaded cell culture's to get a cell
	 * @test If there is no cell in that position, an exception should be thrown
	 */
    Cell& getCellAtPosition(Point position) { return cells.at(position); }

	/**
	 * @brief Gets the total amount of cells in the population, regardless of state.
	 * @return Cells count
	 * @test after generating cells, their population should be same as that
	 * of Cells container (i.e world dimensions)
	 */
    int getTotalCellPopulation() { return cells.size(); }


};

#endif