/**
 * @file GameOfLife.h
 * @ingroup Simulation
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief The Simulation program is structured here
*/

#ifndef GameOfLifeH
#define GameOfLifeH

#include "Cell_Culture/Population.h"
#include "ScreenPrinter.h"
#include <string>

/**
 * @class GameOfLife
 * @ingroup Simulation
 * @brief The heart of the simulation, interconnects the main execution with
 * the graphical interface
 * @details Creates and manages Population and the ScreenPrinter instances.
 * Will instruct the Population of cells to keep updating <BR> as long as the
 * specified number of generations is not reached, and for each iteration
 * <BR>instruct ScreenPrinter to write the information on screen.
 * @see Cell.h
 * @see ScreenPrinter.h
 *
 */
class GameOfLife {

private:
    Population population; /**< Cell population */
    ScreenPrinter& screenPrinter; /**< Print program execution on screen */
    int nrOfGenerations; /**< The number of generations that the cells have lived */

public:
	/**
	 * @brief An instance of the Game of life
	 * @param nrOfGenerations Number of generations as determined by user
	 * @param evenRuleName initiate population with evenRule
	 * @param oddRuleName initiate population with oddRule
	 */
    GameOfLife(int nrOfGenerations, string evenRuleName, string oddRuleName);

	/**
	 * @brief
	 * Run the simulation for as many generations as been set by the user (default = 500).
	 * For each iteration; calculate population changes and print the
	 * information on screen.
	 */
    void runSimulation();

};

#endif
