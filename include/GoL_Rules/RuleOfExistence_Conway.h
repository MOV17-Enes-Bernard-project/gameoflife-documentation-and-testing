/**
 * @file RuleOfExistence_Conway.h
 * @ingroup Rule_of_Existence
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief This file contains conway's rules for existence
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
#define GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H

#include "RuleOfExistence.h"
/**
 * @class RuleOfExistence_Conway
 * @inherit RuleOfExistence
 * @ingroup Rule_of_Existence
 * @brief Conway's RuleOfExistence, applying actions based on PopulationLimits on all 8 surrounding neighbours.
 * @details Concrete RuleOfExistence, implementing Conway's rule of determining alive neighbours
 * <BR>surrounding the cell by checking all 8 directions, 4 x Cardinal + 4 x Diagonal. PopulationLimits are set as;
 * - *UNDERPOPULATION	< 2*	**Cell dies of loneliness**
 * - *OVERPOPULATION	> 3*	**Cell dies of overcrowding**
 * - *RESURRECTION		= 3*	**Cell is infused with life**
 *
 */

class RuleOfExistence_Conway : public RuleOfExistence
{
private:

public:
	/**
	 * @brief Default conway simulation
	 * @param cells
	 */
    RuleOfExistence_Conway(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, ALL_DIRECTIONS, "conway") {}

    ~RuleOfExistence_Conway() {}

	/**
	 * @brief Execute the rule specific for Conway.
	 * @test
	 *  - by providing cells in specific state and executing the rule
	 *  the cells state should change depending on state of the neighbor cells in all directions
	 *  - the name of rule should be conway
	 **/
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_CONWAY_H
