/**
 * @file    RuleOfExistence.h
 * @ingroup Rule_of_Existence
 * @author  Erik Ström
 * @date    October 2017
 * @version 0.1
 * @brief The heart of the the cell population.This file contains the default
 * rule for cell population
*/
#ifndef RULEOFEXISTENCE_H
#define RULEOFEXISTENCE_H

#include<string>
#include<map>
#include<vector>
#include "Cell_Culture/Cell.h"
#include "Support/Globals.h"
using namespace std;

/**
 * @struct PopulationLimits
 * @ingroup Rule_of_Existence
 * @brief Data structure for storing population limits. Used by rules to determine what ACTION to make.
 */
struct PopulationLimits {
    int UNDERPOPULATION, /**< cell dies of loneliness */
            OVERPOPULATION, /**< cell dies of over crowding */
            RESURRECTION; /**< cell lives on is resurrected */
};
/**
 * @struct Directions
 * @brief Data structure for storing directional values. Used by rules to determine neighbouring cells.
 * @ingroup Rule_of_Existence
 */
struct Directions {
    int HORIZONTAL, /**< direction in the x-axis */
		    VERTICAL; /**< direction in the y-axis */
};
/**
 * @brief All cardinal directions on the x-y plane
 **/
const vector<Directions> ALL_DIRECTIONS{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 },{ 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };


/**
 * @brief Cardinal directions; N, E, S, W
 * */
const vector<Directions> CARDINAL{ { 0,-1 },{ 1,0 },{ 0,1 },{ -1,0 } };

/**
* @brief Diagonal directions; NE, SE, SW, NW
* */
const vector<Directions> DIAGONAL{ { 1,-1 },{ 1,1 },{ -1,1 },{ -1,-1 } };

/**
 * @class RuleOfExistence
 * @ingroup Rule_of_Existence
 * @brief Abstract base class, upon which concrete rules will derive.
 * @details The derivations of RuleOfExistence is what determines the
 * culture <BR>of Cell Population. Each rule implements specific behaviours
 * and so may <BR>execute some parts in different orders. In order to
 * accommodate this requirement <BR>RuleOfExistence will utilize a **Template Method**
 * desing pattern, where all derived rules<BR>implements their logic based on the virtual method executeRule().
 */
class RuleOfExistence {
protected:
    string ruleName; /**< Rule to apply */

    map<Point, Cell>& cells; /**< Reference to the population of cells */

    const PopulationLimits POPULATION_LIMITS;/**< Amounts of alive neighbouring cells, with specified limits */

    const vector<Directions>& DIRECTIONS;/**< The directions, by which neighbouring cells are identified */

    /**
     * @brief Get number of cells around the current cell
     * @return the number of live neighbours
     * */
    int countAliveNeighbours(Point currentPoint);

    /**
     * @brief Determine action to taken on cell with respect to its neighbours state
     * @return Action currently assigned to the cell
     */
    ACTION getAction(int aliveNeighbours, bool isAlive);

public:
	/**
	 * @param limits Limit of the population as set by user
	 * @param cells Cells in the culture
	 * @param DIRECTIONS Cardinal directions
	 * @param ruleName The name of the rule to be appplied
	 * @brief Base class constructor for rules of existence
	 */
    RuleOfExistence(PopulationLimits limits,
		    map<Point, Cell>& cells, const vector<Directions>& DIRECTIONS,
		    string ruleName)
            : POPULATION_LIMITS(limits), cells(cells), DIRECTIONS(DIRECTIONS),
              ruleName(ruleName) {}
	/**
 	* @brief Default destructor
 	*/
    virtual ~RuleOfExistence() {}

	/**
	 * @brief Execute rule, in order specific to the concrete rule, by utilizing template method DP
	 */
    virtual void executeRule() = 0;

	/**
	 * @brief Get and return rule to apply
	 * @return Rule to apply
	 */
    string getRuleName() { return ruleName; }
};

#endif