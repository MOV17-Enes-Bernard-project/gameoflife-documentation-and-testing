/**
 * @file RuleOfExistence_VonNeumann.h
 * @ingroup Rule_of_Existence
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief This file contains von_neumann's rules for existence
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
#define GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H

#include "RuleOfExistence.h"
/**
 * @class RuleOfExistence_vonNeumann
 * @inherit RuleOfExistence
 * @ingroup Rule_of_Existence
 * @brief Concrete Rule of existence, implementing Von Neumann's rule.
 * @details Von Neumann's RuleOfExistence, differs from Conway in that only 4 neighbours are accounted for.
 * Only difference from Conway is that neighbours are determined using only
 * cardinal directions (N, E, S, W).
 */
class RuleOfExistence_VonNeumann : public RuleOfExistence /**< Base class governing rules of existence */
{
private:

public:
    /**
     * @param cells
     * @brief Constructor. Creates the RuleOFExistence implementation with specific rule parameters.
     */
    RuleOfExistence_VonNeumann(map<Point, Cell>& cells)
            : RuleOfExistence({ 2,3,3 }, cells, CARDINAL, "von_neumann") {}

    /**
     * @brief Destructor
     */
    ~RuleOfExistence_VonNeumann() {}

	/**
	 * @brief Execute the rule specific Von_neumann
	 * @test
	 *  - by providing cells in specific state and executing the rule
	 *  the cells state should change depending on state of the neighbor cells in cardinal directions
	 *  - the name of rule should be von_neuman
	 */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_VONNEUMANN_H
