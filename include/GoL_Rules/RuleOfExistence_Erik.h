/**
 * @file RuleOfExistence_Erik.h
 * @ingroup Rule_of_Existence
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief This file contains Erik's rules for existence
*/

#ifndef GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
#define GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H

#include "RuleOfExistence.h"

/**
 * @class RuleOfExistence_Erik
 * @inherit RuleOfExistence
 * @ingroup Rule_of_Existence
 * @brief
 * Erik's RuleOfExistence, based on Conway's rule while also differentiate the appearance of cells based on their age.
 * @details
 *Concrete Rule of existence, implementing Erik's rule.
 * Sentient lifeforms is rarely created, but once in a while a cell has lived
 * enough generations to become as wise as Erik.
 * - Once a cell has survived a minimum amount of generations it will recieve
 * a color to distinguish itself from younger ones.
 * - If such a cell would then survive another set amount of generations, it
 * will be marked with a value of **E**.
 * - In the extreme case, where the cell has achieved above requirements and
 * is determined to be the oldest living cell,<BR> it will become a **prime elder**,
 * and have its color changed once again. A generation may only have one such elder.
*/
class RuleOfExistence_Erik : public RuleOfExistence
{
private:
    char usedCellValue;	/**< char value to differentiate very old cells. */

	/**
	 * @brief The eldest cell in the culture. Only one cell can be elder at a
	 * time
	 */
    Cell* primeElder;

	/**
	 * @brief Give cells a mark depending on their age
	 * @details Cells older than 5 generations receive an old age color
	 * If the cell is older than 10 generations, it also gets the value **`E`**
	 * (for Erik) signifying a lifeform of great wisdomw.
	 * @param cell
	 * @param action
	 */
    void erikfyCell(Cell& cell, ACTION action);

	/**
	 * @brief Sets the prime elder, a very rare occasion of a cell surviving longer than any other. Only one cell
can be elder at a time.
	 * @param newElder
	 */
    void setPrimeElder(Cell* newElder);

public:
    /**
     * @param cells
     * @brief Constructor. Creates the RuleOFExistence implementation with specific rule parameters.
     */
    RuleOfExistence_Erik(map<Point, Cell>& cells)
            : RuleOfExistence({2,3,3}, cells, ALL_DIRECTIONS, "erik"), usedCellValue('E') {
        primeElder = nullptr;
    }

    ~RuleOfExistence_Erik() {}

	/**
	 * @brief Execute the rule specific for Erik
	 * * @test
	 *  - by providing cells in specific state and executing the rule
	 *  the cells state should change depending on state of the neighbor cells in all directions
	 *  - the name of rule should be erik
	 *  - cells older than 5 generations should have the OLD color
	 *  - cells older than 10 generations should have value 'E'
	 *  - there should be only one prime elder cell recognized by ELDER color
	 */
    void executeRule();
};

#endif //GAMEOFLIFE_RULEOFEXISTENCE_ERIK_H
