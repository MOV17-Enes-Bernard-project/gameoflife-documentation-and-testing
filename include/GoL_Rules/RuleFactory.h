/**
 * @file    RuleFactory.h
 * @ingroup Rule_of_Existence
 * @author  Erik Ström
 * @date    October 2017
 * @version 0.1
 * @brief This file contains the class where the different rules are determined
*/

#ifndef RULEFACTORY_H
#define RULEFACTORY_H

#include "GoL_Rules/RuleOfExistence.h"

/**
 * @class RuleFactory
 * @brief Singleton class to handle creation of RulesOfExistence objects.
 * @ingroup Rule_of_Existence
 */
class RuleFactory
{
private:
    RuleFactory() {}

public:
	/**
	 * @brief An instance of the rule to be applied to populate cells
	 * @return An instance of a variable (A rule to apply for cell existence)
	 */
    static RuleFactory& getInstance();

	/**
	 * @brief Get the rule of existence to be applied
	 * @details There are three rules
	 * - von_neumann
	 * - erik
	 * - conway
	 * Default rule is conway's original rule. It is applied if neither erik
	 * or von_neumann is specified
	 * @param cells
	 * @param ruleName The rule name that will be applied
	 * @return Pointer to the rule of existence to apply
	 * @test Test that correct rulename is returned
	 */
    RuleOfExistence* createAndReturnRule(map<Point,	Cell>& cells, string ruleName = "conway");

};

#endif