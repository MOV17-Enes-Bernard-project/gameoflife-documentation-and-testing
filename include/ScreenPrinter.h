/**
 * @file ScreenPrinter.h
 * @ingroup Simulation
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief A support class containing functions to print stuff on the screen
 * @details Uses **the terminal** submodule to visually present:
 * * the game on the screeen
 * * help menu
 * * messages to the user
 *
 * 	<br>Read more about
 * 	[**the terminal.**](https://bitbucket.org/MikaelNilsson_Miun/terminal)
*/

#ifndef screenPrinterH
#define screenPrinterH

#include "../terminal/terminal.h"
#include "Cell_Culture/Population.h"

/**
 * @class ScreenPrinter
 * @ingroup Simulation
 * @brief Responsible for visually representing the simulation world on screen.
 * @details Uses **the terminal** submodule to visually present:
 * * the game on the screeen
 * * help menu
 * * messages to the user
 *
 * 	<br>Read more about
 * 	[**the terminal.**](https://bitbucket.org/MikaelNilsson_Miun/terminal)
*/
class ScreenPrinter {

private:
    Terminal terminal;
/**< An auxiliary module for printing colored text and screen sizing.
 * 	<br>Read more about
 * 	[**the terminal.**](https://bitbucket.org/MikaelNilsson_Miun/terminal)
 */

    ScreenPrinter() {}

public:
    /**
     * @brief  Get the unique instance of the ScreenPrinter
     * @return The unique instance of the ScreenPrinter
     */
    static ScreenPrinter& getInstance() {
        static ScreenPrinter instance;
        return instance;
    }

    /**
     * @brief Prints the population to screen
     * @param population Cell population
     */
    void printBoard(Population& population);

    /**
     * @brief Print program help information (usage) on the screen
     */
    void printHelpScreen();

    /**
     * @brief Print some information to the user (any information)
     * @param message information to display on the screen
     */
    void printMessage(string message);

	/**
	 * @brief Uses Terminal function to clear the screen from any inputs and
	 * colors that have been set
	 */
    void clearScreen();
};

#endif
