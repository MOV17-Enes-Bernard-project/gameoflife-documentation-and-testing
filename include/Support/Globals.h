/**
 * @file Globals.h
 * @ingroup Support
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief File with global variables
*/

#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include "SupportStructures.h"

using namespace std;
/**
 * @name Global_Variables
 * @addtogroup Support
 */
extern Dimensions WORLD_DIMENSIONS;
/**<The actual width and height of the used world. This defines the screen
 * size*/

extern string fileName;
/**< File name containing data (cells and coordinates) */


#endif
