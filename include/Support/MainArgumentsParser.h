/**
 * @file MainArgumentsParser.h
 * @ingroup Support
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief Contains functions that have the starting arguments for the program
*/

#ifndef mainArgumentsParserH
#define mainArgumentsParserH

#include "MainArguments.h"
#include <string>
#include <algorithm>
#include <sstream>

using namespace std;

/**
 * @class MainArgumentsParser
 * @ingroup Support
 * @brief Static functions that parses the starting arguments for the application.
 */
class MainArgumentsParser {
public:
    /**
     * @brief Get and run the arguments provided by the user
     * @param argv arguments entered
     * @param length number of characters in the command line
     * @return arguments entered by the user
     * @test
     *  - when not provided with arguments
     *      - then even and odd rules should equal 'conway' and generations should be 100, run simulation should be active
     *  - when provided with -h argument
     *      - then simulation should not be run
     *  - when provided with dimension and generations arguments
     *      - then even and odd rules should equal 'conway',
     *      and generations and world dimensions should match
     *  - when provided with dimension and even rule arguments = 'erik'
     *      - then even and odd rules should equal 'erik', and world dimensions should match
     *  - when provided with dimension and odd rule argument = 'conway',
     *  even rule arguments = 'erik' and filename
     *       - even rule should match 'conway' and odd rule should equal 'erik',
     *       world dimensions should be loaded from the mock file
     */
    ApplicationValues& runParser(char* argv[], int length);

private:
    ApplicationValues appValues; /**<arguments and their values entered */

    /**
     * @brief Checks if a given option exists
     * @param begin Pointer to the beginning of values entered
     * @param end	Pointer to the end of values entered
     * @param option args and values entered
     * @return boolean if a particular argument value is among the inputs
     */
    bool optionExists(char** begin, char** end, const std::string& option);

    /**
     * @brief gets the given option value
     * @param begin pointer to the beginning of option
     * @param end pointer to the end of option
     * @param option the string of arguments and values entered
     * @return the option entered
     */
    char* getOption(char** begin, char** end, const std::string& option);
};

#endif
