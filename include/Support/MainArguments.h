/**
 * @file MainArguments.h
 * @ingroup Support
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief This file contains the command line interface for interacting with
 * the user
*/

#ifndef GAMEOFLIFE_MAINARGUMENTS_H
#define GAMEOFLIFE_MAINARGUMENTS_H

#include "Globals.h"
#include "ScreenPrinter.h"
#include <sstream>
#include <string>

using namespace std;

/**
 * @struct ApplicationValues
 * @ingroup Support
 * @brief Determines the state of the application
 * */
struct ApplicationValues {
    bool runSimulation = true; /**< Returns true if conditions for running simulation are met */
    string evenRuleName, /**< EvenRuleName */
            oddRuleName; /**< OddRuleName */
    int maxGenerations = 100; /**<The maximum generation of the simulation */

};

/**
 * @class BaseArgument
 * @ingroup Support
 * @brief Base class for the command line arguments.
 */
class BaseArgument {
protected:
    const string argValue; /**< Argument value (-h|-g|-s|-f|er|or) */

    void printNoValue();
	/**< inform the user that no value was provided for the argument */

public:
	/**
	 * @brief Constructor for the base class
	 * @param argValue . The command line argument to be executed
	 */
    BaseArgument(string argValue) : argValue(argValue) {}
    virtual ~BaseArgument() {}

	/**
	 * @brief Pure virtual function. Will be implemented differently in the
	 * child clases
	 * @param appValues
	 * @param value
	 */
    virtual void execute(ApplicationValues& appValues, char* value = nullptr) = 0;

	/**
	 * @return commmand line argument
	 */
    const string& getValue() { return argValue; }
};

/**
 * @class HelpArgument
 * @ingroup Support
 * @brief Help screen
 */
class HelpArgument : public BaseArgument {
public:
	/**
	 * @brief get help information about the program with ```-h```
	 */
    HelpArgument() : BaseArgument("-h") {}
    ~HelpArgument() {}

    /**
     * @test
     *  - test that the command line argument is '-h'
     */
    void execute(ApplicationValues& appValues, char* value);
};

/**
 * @class GenerationsArgument
 * @ingroup Support
 * @brief Amount of generations to simulate
 */
class GenerationsArgument : public BaseArgument {
public:
	/**
	 * @brief Set how many generations the program should run with ```-g```
	 */
    GenerationsArgument() : BaseArgument("-g") {}
    ~GenerationsArgument() {}

    /**
     * @test
     *  - test that the command line argument is '-g'
     *  - test that the number of generations is initiated for the application values as provided by the argument
     */
    void execute(ApplicationValues& appValues, char* generations);
};

/**
 * @class WorldsizeArgument
 * @ingroup Support
 * @brief Custom population size
 */
class WorldsizeArgument : public BaseArgument {
public:
	/**
	 * @brief Set the population size with ```-s```
	 */
    WorldsizeArgument() : BaseArgument("-s") {}

    ~WorldsizeArgument() {}

    /**
     * @test
     *  - test that the command line argument is '-s'
     *  - test that dimensions are initiated as provided by the argument
     */
    void execute(ApplicationValues& appValues, char* dimensions);
};

/**
 * @class FileArgument
 * @ingroup Support
 * @brief Initiate population from file
 */
class FileArgument : public BaseArgument {
public:
	/**
	 * @brief Set file to read data from using  ```-f``` and file name
	 */
    FileArgument() : BaseArgument("-f") {}
    ~FileArgument() {}

    /**
     * @test
     *  - test that the command line argument is '-f'
     *  - if provided with filename
     *      - test that the file name matches
     *      - test that run application parameter is activated
     *  - if NOT provided with filename
     *      - test that the fileName argument is not set
     *      - test that run application parameter is de-activated
     */
    void execute(ApplicationValues& appValues, char* fileNameArg);
};

/**
 * @class EvenRuleArgument
 * @ingroup Support
 * @brief Rule used for even generations
 */
class EvenRuleArgument : public BaseArgument {
public:
	/**
	 * @brief Set program to run the even rule by entering ```er```
	 */
    EvenRuleArgument() : BaseArgument("-er") {}
    ~EvenRuleArgument() {}

    /**
     * @test
     *  - test that the command line argument is '-er'
     *  - test that the even rule name is matching the provided literal
     */
    void execute(ApplicationValues& appValues, char* evenRule);
};

/**
 * @class OddRuleArgument
 * @ingroup Support
 * @brief Rule used for odd generations
 */
class OddRuleArgument : public BaseArgument {
public:
	/**
	 * @brief Set the program to run the odd rule by entering ```or```
	 */
    OddRuleArgument() : BaseArgument("-or") {}
    ~OddRuleArgument() {}

    /**
    * @test
    *  - test that the command line argument is '-or'
    *  - test that the even rule name is matching the provided literal
    */
    void execute(ApplicationValues& appValues, char* oddRule);
};

#endif //GAMEOFLIFE_MAINARGUMENTS_H
