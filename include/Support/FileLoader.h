/**
 * @file FileLoader.h
 * @ingroup Support
 * @author Erik Ström
 * @date October 2017
 * version 0.1
 * @brief This has the file loader that reads startup values from a file
*/

#ifndef FileLoaderH
#define FileLoaderH

#include <map>
#include "Cell_Culture/Cell.h"
#include "Globals.h"

using namespace std;

/**
 * @class FileLoader
 * @ingroup Support
 * @brief Determines starting values for simulation, based on contents of specified file.
 * @details Reads startup values from specified file, containing values for WORLD_DIMENSIONS and cell Population.
 * Will create the corresponding cells.
 */
class FileLoader {

public:
    FileLoader() {}

	/**
	 * @brief Load the given map with cells from the file pointed by the
	 * global variable filename
	 * @param cells
	 * @throws Throws an exception if the file does not exist and closes the
	 * program as a result
	 * @test
	 * - Check that file loader loads files correctly
	 * - Check that it throws an exception when the file is not available
	 * (wrong file name or no file exist)
	 */
    void loadPopulationFromFile(map<Point, Cell>& cells);

};

#endif
