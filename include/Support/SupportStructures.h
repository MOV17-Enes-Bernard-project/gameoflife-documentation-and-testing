/**
 * @file SupportStructures.h
 * @ingroup Support
 * @author Erik Ström
 * @date October 2017
 * @version 0.1
 * @brief Contains functions that hold the the coordinates as well as the
 * dimension of the screen
*/

#ifndef GAMEOFLIFE_SUPPORTSTRUCTURES_H
#define GAMEOFLIFE_SUPPORTSTRUCTURES_H

/**
 * @struct Point
 * @ingroup Support
 * @brief Constitutes a single Point in the simulated world.
 * @details The Point structure handles x and y (column/row) coordinates in
 * the <BR> world of Game of life, and is used to map Cell objects to their positions.
 **/

struct Point {
    int x, /**< The x-coordinate */
		    y; /**< The y-coordinate */

    /**
     * @brief Overloading operator < for comparing Point objects
     * @param other The other point to compare with
     * @return True if other point is less than the current one
     */
    bool operator < (const Point& other) const {
        if (x == other.x)
            return y < other.y;
        return x < other.x;
    }

};

/**
 * @struct Dimensions
 * @ingroup Support
 * @brief Data structure holding information about world dimensions in pixels.
 **/
struct Dimensions {
    int WIDTH, /**< The width of the screen (display) */
		    HEIGHT; /**< The height of the screen (display) */
};


#endif //GAMEOFLIFE_SUPPORTSTRUCTURES_H
